package com.example.alphabetapp.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.alphabetapp.R;

public class TestResultActivity extends AppCompatActivity {

    public static final String PREFS_KEY = "record";
    public static final String PREFS_NAME = "myPrefs";

    private TextView textViewTestResult;
    private TextView textViewRecord;

    private String testResult;

    private int record;
    private int result;

    private Button buttonRestart;

    private SharedPreferences sharedPreferences;

    /**
     * Метод, который вызывается при создании нового экземпляра активности. Вызывается один раз,
     * когда происходит инициализация.
     *
     * @param savedInstanceState объект, содержащий сохраненную информацию о состоянии.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializationUI();
        getMyIntent();
        getMyRecord();
        putResultToPreferencies();

        viewResults();
        viewRecord();
    }

    /**
     * Инициализирует пользовательский интерфейс.
     */
    private void initializationUI() {
        setContentView(R.layout.activity_test_result);
        textViewTestResult = findViewById(R.id.textViewTestResult);
        textViewRecord = findViewById(R.id.textViewRecord);
        buttonRestart = findViewById(R.id.buttonRestart);
    }

    /**
     * Получает данные о текущем результате из активности тестирования.
     */
    private void getMyIntent() {
        Intent intent = getIntent();
        testResult = intent.getStringExtra(TestLettersActivity.INTENT_NAME);
    }

    /**
     * Получает сведения о рекорде.
     */
    private void getMyRecord() {
        sharedPreferences =
                getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        record = sharedPreferences.getInt(PREFS_KEY, 0);
    }

    /**
     * Заменяет данные о текущем рекорде.
     */
    private void putResultToPreferencies() {
        result = Integer.parseInt(testResult);
        if (result > record) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("record", result);
            editor.apply();
        }
    }

    /**
     * Устанавливает текущий рекорд во view.
     */
    private void viewRecord() {
        textViewRecord.setText(String.format("Твой текущий рекорд\n %s из %s", record, 10));
    }

    /**
     * Выводит текущий результат на экран.
     */
    private void viewResults() {
        switch (result) {
            case 1:
            case 2:
            case 3:
                setTextOnTextView(testResult, "Ты жульничаешь! Попробуй не угадывать!");
                setTextOnButton("Попробуй еще раз");
                break;
            case 4:
            case 5:
            case 6:
                setTextOnTextView(testResult, "Хороший результат, но ты можешь лучше!");
                setTextOnButton("Побить свой рекорд");
                break;
            case 7:
            case 8:
                setTextOnTextView(testResult, "Ты знаешь уже много букв, но можешь еще лучше");
                setTextOnButton("Закрепить результат");
                break;
            case 9:
            case 10:
                setTextOnTextView(testResult, "Отличный результат! Ты молодец!");
                setTextOnButton("Закрепить результат");
        }
    }

    /**
     * Реагирует на нажатия и возвращает в активность тестирования.
     *
     * @param view ссылка на view.
     */
    public void onClickRestart(View view) {
        Intent intent = new Intent(this, TestLettersActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Устанавливает текст на кнопке.
     *
     * @param text ссылка на строку.
     */
    private void setTextOnButton(String text) {
        buttonRestart.setText(text);
    }

    /**
     * Устанавливает текст о результате.
     *
     * @param result ссылка на результат.
     * @param wish   ссылка на пожелание.
     */
    private void setTextOnTextView(String result, String wish) {
        textViewTestResult.setText(String.format("Твой результат %s,\n%s", result, wish));
    }
}
