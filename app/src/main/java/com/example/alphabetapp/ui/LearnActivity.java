package com.example.alphabetapp.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alphabetapp.R;

/**
 * Активность для изучения алфавита.
 * TODO: продумать логику и реализовать.
 */
public class LearnActivity extends AppCompatActivity {

//    private TextView textViewLetter;
//    private ImageView imageViewPic;
//    private Button buttonNext;
//    private Button buttonRestart;
//    private Button buttonPrev;

    /**
     * Метод, который вызывается при создании нового экземпляра активности. Вызывается один раз,
     * когда происходит инициализация.
     *
     * @param savedInstanceState объект, содержащий сохраненную информацию о состоянии.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
    }

    /**
     * Метод инициализирующий пользовательский интерфейс.
     */
    private void initUI() {
        setContentView(R.layout.activity_learn);
//        textViewLetter = findViewById(R.id.textViewLetter);
//        imageViewPic = findViewById(R.id.imageViewPic);
//        buttonPrev = findViewById(R.id.buttonPrev);
//        buttonRestart = findViewById(R.id.buttonRestart);
//        buttonNext = findViewById(R.id.buttonNext);

    }

    /**
     * Метод реализующий нажатия на кнопки.
     *
     * @param view ссылка на view.
     */
    public void onClickActivLearn(View view) {
//        switch (view.getId()) {
//            case R.id.buttonPrev:
//
//                break;
//            case R.id.buttonRestart:
//
//                break;
//            case R.id.buttonNext:
//
//        }
    }
}
