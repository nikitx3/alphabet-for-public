package com.example.alphabetapp.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.alphabetapp.R;

/**
 * Активность запускается при старте приложения, представляет собой меню выбора. Сейчас возможно
 * только тестирование на знание алфавита, в дальнейшем будет добавлено изучение.
 */
public class MainActivity
        extends AppCompatActivity {

    /**
     * Метод, который вызывается при создании нового экземпляра активности. Вызывается один раз,
     * когда происходит инициализация.
     *
     * @param savedInstanceState объект, содержащий сохраненную информацию о состоянии.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Реализует нажатия на кнопки в активности.
     *
     * @param view ссылка на текущий view.
     */
    public void onClickStart(View view) {
        Intent intent = new Intent(this, TestLettersActivity.class);
        startActivity(intent);
        finish();
    }

}
