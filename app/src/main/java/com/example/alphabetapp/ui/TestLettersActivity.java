package com.example.alphabetapp.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.alphabetapp.R;
import com.example.alphabetapp.data.LetterRus;
import com.example.alphabetapp.model.Letter;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.valueOf;

/**
 * Активность - тест на знание алфавита.
 */
public class TestLettersActivity
        extends AppCompatActivity
        implements SoundPool.OnLoadCompleteListener {

    public static final String INTENT_NAME = "countRightAnswers";
    private static final long ANIMATION_TIME = 3000;

    private ImageView imageViewForQuestion;
    private TextView textViewProgress;
    private ProgressBar progressBar;
    private TextView textViewDesc;

    private Button button0;
    private Button button1;
    private Button button2;
    private Button button3;

    private int numberOfQuestion;
    private int numberOfButtonWithRightAnswer;
    private int counter = 0;
    private int counterWriteAnswers = 0;

    private static final int MAX = 10;
    private static final int MAX_STREAMS = 1;

    private List<Letter> lettersList;
    private ArrayList<Button> buttonsList;
    private ArrayList<Integer> currentQuestionList;

    private SoundPool soundPool;
    private int soundIdShot;


    /**
     * Метод, который вызывается при создании нового экземпляра активности. Вызывается один раз,
     * когда происходит инициализация.
     *
     * @param savedInstanceState объект, содержащий сохраненную информацию о состоянии.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            numberOfQuestion = savedInstanceState.getInt("numberOfQuestion");
            counter = savedInstanceState.getInt("counter");
            counterWriteAnswers = savedInstanceState.getInt("counterWriteAnswers");

            initUI();
            refresh();
            getContent();
            startLearning();
            onClickChoose();
        } else {
            initUI();
            refresh();
            getContent();
            generateQuestion();
            startLearning();
            onClickChoose();
        }
    }

    /**
     * Переопределяет метод суперкласса. Вызывается после завершения загрузки звука.
     *
     * @param soundPool объект SoundPool из метода load()
     * @param sampleId  идентификатор образца загруженного звука.
     * @param status    состояние операции загрузки (0 = успех)
     */
    @Override
    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
    }

    /**
     * Переопределяет метод суперкласса. Сохраняет все соответствующие состояния фрагментов.
     *
     * @param outState ссылка на бандл.
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("numberOfQuestion", numberOfQuestion);
        outState.putInt("counter", counter);
        outState.putInt("counterWriteAnswers", counterWriteAnswers);

    }

    /**
     * Инициализирует пользовательский интерфейс.
     */
    private void initUI() {
        setContentView(R.layout.activity_test_letters);

        imageViewForQuestion = findViewById(R.id.imageViewForQuestion);
        textViewProgress = findViewById(R.id.textViewProgress);
        textViewProgress.setText(valueOf(counter + 1));
        textViewDesc = findViewById(R.id.textViewDesc);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setMax(MAX);
        progressBar.setProgress(counter);

        currentQuestionList = new ArrayList<>();

        button0 = findViewById(R.id.button0);
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);

        buttonsList = new ArrayList<>();
        buttonsList.add(button0);
        buttonsList.add(button1);
        buttonsList.add(button2);
        buttonsList.add(button3);

        soundPool = new SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, 0);
    }

    /**
     * Обновляет интерфейс после изменений, вызванных нажатием на нопки, анимацией, изменением
     * заднего фона.
     */
    private void refresh() {
        button0.setBackgroundResource(R.drawable.button_blue);
        button1.setBackgroundResource(R.drawable.button_blue);
        button2.setBackgroundResource(R.drawable.button_blue);
        button3.setBackgroundResource(R.drawable.button_blue);

        button0.setTextColor(this.getResources().getColor(R.color.colorBlack));
        button1.setTextColor(this.getResources().getColor(R.color.colorBlack));
        button2.setTextColor(this.getResources().getColor(R.color.colorBlack));
        button3.setTextColor(this.getResources().getColor(R.color.colorBlack));

        textViewDesc.setText("");

        button0.setEnabled(true);
        button1.setEnabled(true);
        button2.setEnabled(true);
        button3.setEnabled(true);

        button0.animate().alpha(1f).setDuration(0);
        button1.animate().alpha(1f).setDuration(0);
        button2.animate().alpha(1f).setDuration(0);
        button3.animate().alpha(1f).setDuration(0);
    }

    /**
     * Запускает тестирование.
     */
    private void startLearning() {
        if (currentQuestionList.contains(numberOfQuestion)) {
            for (int c = 0; currentQuestionList.contains(numberOfQuestion); c++) {
                generateQuestion();
            }
        }
        imageViewForQuestion.setImageResource(getResources()
                .getIdentifier("drawable/" + lettersList.get(numberOfQuestion)
                        .getImage(), null, getPackageName()));
        soundIdShot = soundPool.load(this, getResources()
                .getIdentifier("raw/" + lettersList
                        .get(numberOfQuestion)
                        .getSound(), null, getPackageName()), 1);

        ArrayList<Integer> wrongAnswersList = new ArrayList<>();
        for (int i = 0; i < buttonsList.size(); i++) {
            if (i == numberOfButtonWithRightAnswer) {
                buttonsList.get(i).setText(String.valueOf(
                        lettersList.
                                get(numberOfQuestion).
                                getLetter()));
            } else {
                int numberWrongAnswer = generateWrongAnswer();
                if (numberWrongAnswer == numberOfQuestion
                        || wrongAnswersList.contains(numberWrongAnswer)) {
                    for (int j = 0; numberWrongAnswer == numberOfQuestion
                            || wrongAnswersList.contains(numberWrongAnswer); j++) {
                        numberWrongAnswer = generateWrongAnswer();
                    }
                }
                wrongAnswersList.add(numberWrongAnswer);
                buttonsList.get(i).setText(String.valueOf(lettersList
                        .get(numberWrongAnswer)
                        .getLetter()));
            }
        }
        currentQuestionList.add(numberOfQuestion);
    }

    /**
     * Присваивает случайное число номеру ответа и номеру кнопки с правильным ответом.
     */
    private void generateQuestion() {
        numberOfQuestion = (int) (Math.random() * lettersList.size());
        numberOfButtonWithRightAnswer = (int) (Math.random() * buttonsList.size());
    }

    /**
     * Присваивает случайное число для номера неправильного ответа.
     *
     * @return значение int  номером неправильного ответа.
     */
    private int generateWrongAnswer() {
        return (int) (Math.random() * lettersList.size());
    }

    /**
     * Получает лист с объектами класса Letter.
     */
    private void getContent() {
        lettersList = new LetterRus().getListRus();
    }

    /**
     * Реагирует на нажатия на кнопки.
     */
    public void onClickChoose() {
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(0);
                if (numberOfButtonWithRightAnswer == 0) {
                    button0.setBackgroundResource(R.drawable.button_green);
                } else {
                    button0.setBackgroundResource(R.drawable.button_red);
                }
                if (numberOfButtonWithRightAnswer == 1) {
                    button1.setBackgroundResource(R.drawable.button_green);
                    animateButton(button1);
                } else if (numberOfButtonWithRightAnswer == 2) {
                    button2.setBackgroundResource(R.drawable.button_green);
                    animateButton(button2);
                } else if (numberOfButtonWithRightAnswer == 3) {
                    button3.setBackgroundResource(R.drawable.button_green);
                    animateButton(button3);
                }
                setDisabledButton();
                soundPool.play(soundIdShot, 1, 1, 0, 0, 1);
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(1);
                if (numberOfButtonWithRightAnswer == 1) {
                    button1.setBackgroundResource(R.drawable.button_green);
                } else {
                    button1.setBackgroundResource(R.drawable.button_red);
                }
                if (numberOfButtonWithRightAnswer == 0) {
                    button0.setBackgroundResource(R.drawable.button_green);
                    animateButton(button0);
                } else if (numberOfButtonWithRightAnswer == 2) {
                    button2.setBackgroundResource(R.drawable.button_green);
                    animateButton(button2);
                } else if (numberOfButtonWithRightAnswer == 3) {
                    button3.setBackgroundResource(R.drawable.button_green);
                    animateButton(button3);
                }
                setDisabledButton();
                soundPool.play(soundIdShot, 1, 1, 0, 0, 1);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(2);
                if (numberOfButtonWithRightAnswer == 2) {
                    button2.setBackgroundResource(R.drawable.button_green);
                } else {
                    button2.setBackgroundResource(R.drawable.button_red);
                }
                if (numberOfButtonWithRightAnswer == 1) {
                    button1.setBackgroundResource(R.drawable.button_green);
                    animateButton(button1);
                } else if (numberOfButtonWithRightAnswer == 0) {
                    button0.setBackgroundResource(R.drawable.button_green);
                    animateButton(button0);
                } else if (numberOfButtonWithRightAnswer == 3) {
                    button3.setBackgroundResource(R.drawable.button_green);
                    animateButton(button3);
                }
                setDisabledButton();
                soundPool.play(soundIdShot, 1, 1, 0, 0, 1);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(3);
                if (numberOfButtonWithRightAnswer == 3) {
                    button3.setBackgroundResource(R.drawable.button_green);
                } else {
                    button3.setBackgroundResource(R.drawable.button_red);
                }
                if (numberOfButtonWithRightAnswer == 1) {
                    button1.setBackgroundResource(R.drawable.button_green);
                    animateButton(button1);
                } else if (numberOfButtonWithRightAnswer == 2) {
                    button2.setBackgroundResource(R.drawable.button_green);
                    animateButton(button2);
                } else if (numberOfButtonWithRightAnswer == 0) {
                    button0.setBackgroundResource(R.drawable.button_green);
                    animateButton(button0);
                }
                setDisabledButton();
                soundPool.play(soundIdShot, 1, 1, 0, 0, 1);
            }
        });
    }

    /**
     * Устанавливает запрет на нажатия на клавиши. Прпименяется после выбора, пока не появился
     * новый ответ.
     */
    private void setDisabledButton() {
        button0.setEnabled(false);
        button1.setEnabled(false);
        button2.setEnabled(false);
        button3.setEnabled(false);
    }

    /**
     * Устанавливает анимацию на кнопки.
     *
     * @param button ссылка на кнопку.
     */
    private void animateButton(Button button) {
        if (button.getId() != button0.getId()) {
            button0.animate().alpha(0).setDuration(ANIMATION_TIME);
        }
        if (button.getId() != button1.getId()) {
            button1.animate().alpha(0).setDuration(ANIMATION_TIME);
        }
        if (button.getId() != button2.getId()) {
            button2.animate().alpha(0).setDuration(ANIMATION_TIME);
        }
        if (button.getId() != button3.getId()) {
            button3.animate().alpha(0).setDuration(ANIMATION_TIME);
        }
    }

    /**
     * Проверяет, правильный ли ответ выбран пользователем.
     *
     * @param buttonNumber ссылка на номер кнопки.
     */
    private void checkAnswer(int buttonNumber) {
        if (buttonNumber == numberOfButtonWithRightAnswer) {
            setTextWithRedFirstLetter();
            counterWriteAnswers++;
        } else {
            setTextWithRedFirstLetter();
        }
        doAfterClick();
    }

    /**
     * Устанавливает цвет текста на первую букву.
     */
    private void setTextWithRedFirstLetter() {
        String deck = lettersList.get(numberOfQuestion).getDescription();
        String firstLetter = deck.substring(0, 1);
        String lastLetters = deck.substring(1);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        SpannableString spannableStringFirst = new SpannableString(firstLetter);
        SpannableString spannableStringLast = new SpannableString(lastLetters);
        spannableStringFirst.setSpan(new ForegroundColorSpan(Color.RED), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.append(spannableStringFirst);
        spannableStringBuilder.append(spannableStringLast);
        textViewDesc.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);
    }

    /**
     * Реализует действия после нажатия на кнопку.
     */
    private void doAfterClick() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                counter++;
                if (counter != MAX) {
                    textViewProgress.setText(valueOf(counter + 1));
                    startLearning();
                } else {
                    textViewProgress.setText(valueOf(MAX));
                    goToResult();
                }
                progressBar.setProgress(counter + 1);
                refresh();
            }
        }, 3000);
    }

    /**
     * Реализует намерение перейти на активность с результатами.
     */
    private void goToResult() {
        Intent intent = new Intent(this, TestResultActivity.class);
        intent.putExtra(INTENT_NAME, valueOf(counterWriteAnswers));
        startActivity(intent);
        finish();
    }
}
