package com.example.alphabetapp.data;

import com.example.alphabetapp.model.Letter;

import java.util.ArrayList;
import java.util.List;

public class LetterRus extends Letter {

    public LetterRus() {
    }

    public List<Letter> getListRus() {
        List<Letter> lettersList = new ArrayList<>();
        lettersList.add(new Letter('А', "Арбуз", "let_01", "let_01"));
        lettersList.add(new Letter('Б', "Банан", "let_02", "let_02"));
        lettersList.add(new Letter('В', "Виноград", "let_03", "let_03"));
        lettersList.add(new Letter('Г', "Груша", "let_04", "let_04"));
        lettersList.add(new Letter('Д', "Дом", "let_05", "let_05"));
        lettersList.add(new Letter('Е', "Енот", "let_06", "let_06"));
        lettersList.add(new Letter('Ё', "Ёж", "let_07", "let_07"));
        lettersList.add(new Letter('Ж', "Желудь", "let_08", "let_08"));
        lettersList.add(new Letter('З', "Заяц", "let_09", "let_09"));
        lettersList.add(new Letter('И', "Индюк", "let_10", "let_10"));
        lettersList.add(new Letter('Й', "Йогурт", "let_11", "let_11"));
        lettersList.add(new Letter('К', "Кит", "let_12", "let_12"));
        lettersList.add(new Letter('Л', "Лягушка", "let_13", "let_13"));
        lettersList.add(new Letter('М', "Медведь", "let_14", "let_14"));
        lettersList.add(new Letter('Н', "Носорог", "let_15", "let_15"));
        lettersList.add(new Letter('О', "Облако", "let_16", "let_16"));
        lettersList.add(new Letter('П', "Поезд", "let_17", "let_17"));
        lettersList.add(new Letter('Р', "Рак", "let_18", "let_18"));
        lettersList.add(new Letter('С', "Сова", "let_19", "let_19"));
        lettersList.add(new Letter('Т', "Торт", "let_20", "let_20"));
        lettersList.add(new Letter('У', "Улитка", "let_21", "let_21"));
        lettersList.add(new Letter('Ф', "Фламинго", "let_22", "let_22"));
        lettersList.add(new Letter('Х', "Хамелеон", "let_23", "let_23"));
        lettersList.add(new Letter('Ц', "Ципленок", "let_24", "let_24"));
        lettersList.add(new Letter('Ч', "Черепаха", "let_25", "let_25"));
        lettersList.add(new Letter('Ш', "Шар", "let_26", "let_26"));
        lettersList.add(new Letter('Щ', "Щука", "let_27", "let_27"));
        lettersList.add(new Letter('Ъ', "Твердый знак", "let_28", "let_28"));
        lettersList.add(new Letter('Ы', "Сыр", "let_29", "let_29"));
        lettersList.add(new Letter('ь', "Мягкий знак", "let_30", "let_30"));
        lettersList.add(new Letter('Э', "Экскаватор", "let_31", "let_31"));
        lettersList.add(new Letter('Ю', "Юла", "let_32", "let_32"));
        lettersList.add(new Letter('Я', "Яблоко", "let_33", "let_33"));
        return lettersList;
    }
}
