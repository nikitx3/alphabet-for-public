package com.example.alphabetapp.model;

public class Letter {
    private char letter;
    private String description;
    private String image;
    private String sound;

    public Letter(char letter, String description, String image, String sound) {
        this.letter = letter;
        this.description = description;
        this.image = image;
        this.sound = sound;
    }

    protected Letter() {
    }

    public char getLetter() {
        return letter;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public String getSound() {
        return sound;
    }
}

